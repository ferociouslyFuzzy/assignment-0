package edu.usu.cs.oo;

public class Company {

	private String _name;
	private Location _location;
	
	Company(String name, Location location)
	{
		_name = name;
		_location = location;
	}

    public String GetName(){return _name;}
    public Location GetLocation(){return _location;}

    public void SetName(String name){_name = name;}
	public void SetLocation(Location location){_location = location;}

    @Override
	public String toString()
	{
		StringBuilder output = new StringBuilder();
		output.append(GetName()+" at: \n")
		      .append(GetLocation().toString());
		
		return output.toString();
		
	}
	

}
