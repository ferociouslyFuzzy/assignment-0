package edu.usu.cs.oo;

public class Job {

	private String _jobTitle;
	private int _basePay;
	private Company _company;
    private String _wantToWorkFor = "To work for ";
    private String _makingHowMuch = " making ";
    private String _title = "as a ";
    private String _yearly = " dollars per year";
	
	public Job(String jobTitle, int basePay, Company company)
	{
		_jobTitle = jobTitle;
		_basePay = basePay;
		_company = company;
	}

    public void SetJobTitle(String jobTitle){_jobTitle = jobTitle;}
    public void SetBasePay(int basePay){_basePay = basePay;}
    public void SetCompany(Company company){_company = company;}

    public String GetJobTitle(){return _jobTitle;}
    public int GetBasePay(){return _basePay;}
    public Company GetCompany(){return _company;}

	@Override
	public String toString()
	{
		return _wantToWorkFor + GetCompany() +_title + GetJobTitle() + _makingHowMuch + GetBasePay() + _yearly;
	}
}
