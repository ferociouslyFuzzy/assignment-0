package edu.usu.cs.oo;

public class Student {
	
	private String _name;
	private String _ANumber;
	private Job _dreamJob;
	
	public Student(String name, String ANumber, Job dreamJob)
	{
		this._name = name;
		this._ANumber = ANumber;
		this._dreamJob = dreamJob;
	}

	public String getName(){return _name;}
    public String getANumber() {return _ANumber;}
    public Job getDreamJob(){return _dreamJob;}

	public void setName(String name){this._name = name;}
	public void setANumber(String aNumber){_ANumber = aNumber;}
	public void SetDreamJob(Job dreamJob){this._dreamJob = dreamJob;}
	
	@Override
	public String toString()
	{
        StringBuilder output = new StringBuilder();

        output.append("Name: ")
              .append(getName()+"\n")
              .append("A#: ")
              .append(getANumber()+"\n")
              .append("Dream Job: ")
              .append(getDreamJob().toString()+"\n");


		
		return output.toString();
	}
	
}
