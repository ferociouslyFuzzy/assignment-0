package edu.usu.cs.oo;

import java.io.*;

public class Driver {
	
	public static void main(String[] args) throws IOException {
		Student student = new Student("Chris Thatcher", "A0123844",
                          new Job("Developer", 100000,
                          new Company("Blizzard Entertainment",
                          new Location("1500 S 1000 W", "84321", "Logan", "Utah"))));

        System.out.println(student.toString());

        Student studentTwo = new Student("Joshua Furman", "AL33T!!",
                             new Job("Software Engineer", 70000,
                             new Company("CSC",
                             new Location("1300 N 200 E #118", "84321", "Logan","Utah"))));

        System.out.println(studentTwo);

        File file = new File("Output.txt");

        if(!file.exists())
             file.createNewFile();
        FileWriter fout = new FileWriter(file.getAbsoluteFile());
        BufferedWriter buffFout = new BufferedWriter(fout);

        buffFout.write(studentTwo.toString());

        buffFout.close();
    }

}