package edu.usu.cs.oo;

public class Location {

	private String _streetAddress;
	private String _areaCode;
	private String _city;
	private String _state;
	
	Location(String addr, String areaCode, String city, String state)
	{
		_streetAddress = addr;
		_areaCode = areaCode;
		_city = city;
		_state = state;
	}

	public void SetStreetAddress(String addr)
	{
		_streetAddress = addr;
	}
	public void SetAreaCode(String areaCode)
	{
		_areaCode = areaCode;
	}
	public void SetCity(String city)
	{
		_city = city;
	}
	public void SetState(String state)
	{
		_state = state;
	}
	
	public String GetStreetAddress()
	{
		return _streetAddress;
	}
	public String GetAreaCode()
	{
		return _areaCode;
	}
	public String GetCity()
	{
		return _city;
	}
	public String GetState()
	{
		return _state;
	}
	
	@Override
    public String toString()
    {
        StringBuilder output = new StringBuilder();

        output.append(GetStreetAddress()+"\n")
              .append(GetAreaCode()+"\n")
              .append(GetCity()+"\n")
              .append(GetState()+"\n");
        return output.toString();
    }
	
	
	
	
}
